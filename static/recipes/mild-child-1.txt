Recipe: Mild Child #1
--------------------------
Brewer: Chris Baker
Asst Brewer: Chris Asa
Style: Mild
TYPE: All Grain

Ingredients:
------------
Amt             Name                                     Type          #        %/IBU         
6 lbs           Maris Otter (Crisp) (4.0 SRM)            Grain         1        85.7 %        
4.0 oz          Black (Patent) Malt (500.0 SRM)          Grain         2        3.6 %         
4.0 oz          Caramel/Crystal Malt - 60L (60.0 SRM)    Grain         3        3.6 %         
4.0 oz          Caramel/Crystal Malt -120L (120.0 SRM)   Grain         4        3.6 %         
4.0 oz          Chocolate Malt (350.0 SRM)               Grain         5        3.6 %         
1.00 oz         Willamette [5.50 %] - Boil 60.0 min      Hop           6        18.6 IBUs
0.50 Items      Whirlfloc Tablet (Boil 15.0 mins)        Fining        7        -             
1.0 pkg         English Ale (White Labs #WLP002) [35.49  Yeast         8        -             

Mash Schedule: Single Infusion, Full Body, Batch Sparge
Total Grain Weight: 7 lbs
----------------------------
Name              Description                             Step Temperat Step Time     
Mash In           Add 8.75 qt of water at 172.0 F         156.0 F       45 min        

Sparge: Batch sparge with 3 steps (1.01gal, 2.35gal, 2.35gal) of 168.0 F water

Notes:
----------------------------
From: http://byo.com/body/item/1893-mild-ale-style-profile

Starter started on Saturday, Jan 21, on stirplate, at 65°F

11:48 - began heating 4gal water
12:05 - water at 160°F
turned off burner because malt wasn’t crushed
crushed malt
added another gallon, for a total of 5gal in the kettle
mashed with 10qt

Fermentation set for 67°F

Wort RI is 9.4 brix/1.037
Sample Tuesday afternoon (~42 hours) is 6%, adjusted gravity is 1.015 and 2.82% ABV
Turned temp briefly up to 69°F, then changed my mind and dropped to 68°F
Sample was small, tasted good.

Sample Thursday afternoon (~66 hours) is 5.5%, adjusted gravity is 1.012 and 3.22% ABV
Turned temp to 69°F, but it was already at and presumably has been at 69°F, so I don’t imagine this did anything.

Crashed to 55°F on Feb 3.

Final gravity on Feb 11 (: 1.012 @ 69°F => 1.012
Racked to keg, set temp to 55°F and CO2 to 10 psi

Created with BeerSmith 2 - http://www.beersmith.com
-------------------------------------------------------------------------------------
