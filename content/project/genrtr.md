+++
title = "GenRTR"
date = "2017-01-02T13:49:17-05:00"
name = "genrtr"
type = "project"
description = "A trust-region method for optimization over Riemannian manifolds."
actions = false
+++

Placeholder page for GenRTR: http://www.math.fsu.edu/~cbaker/GenRTR/
