---
title: "Tallahassee Green Chile Harvest 2017"
date: "2017-09-05T23:04:00-04:00"
categories: 
- kitchen
thumbnailImagePosition: left
thumbnailImage: images/green-chile-2017/6-red-v-green-tn.jpg
keywords: 
- green chile
tags: 
- green chile
- cooking
- recipes
---

Of all the things I miss from living in Albuquerque, the two most significant are the weather and New Mexico chiles.<!--more-->

Living in New Mexico, chile is ever-present. Driving into the state on I-40, you'll see billboards advertising green
chile cheeseburgers almost as soon as you cross the state line. Flying into the ABQ International Sunport (yeah,
[sunport](https://en.wikipedia.org/wiki/Albuquerque_International_Sunport)), you'll even smell them.  It's my theory
that each American airport has its characteristic smell, that terroir over the universal base of over-cooked coffee and
Auntie Anne's pretzels. In Austin, it's bar-b-q. In Tallahassee, it's Axe body spray. In Albuquerque, you can smell
fresh tortillas and chile as soon as you deplane.

And supposing you didn't notice all of this, someone is going to bring it up. It will be part of your welcome
orientation: drink lots of water; say no to drugs; let me tell you about the miracle of New Mexico chiles. They make such a big deal out of it, and
if you're as cynical as I am you might assume it's a local quirk, a tourist gimmick like the tequila worm in
Cancun or New York's cronuts. Admittedly, there _are_ folks who don't like chile. Of course, there's folks who don't like
bacon or alcohol or any number of wonderful things. Who can understand those people? Moving on.

So you go out, see what this chile thing is all about. And you eat a ton of it, because it is awesome and you recognize
that. But after your indoctrination, where you eat chile at every meal for two weeks, you'll eventually settle into a
more normal routine, where you only eat chile with two meals a day. Because it's present at every restauarant,
appropriate for every meal.  You start the day with a NM breakfast burrito (eggs, potatoes, cheese, chile and something
from a pig), huevos rancheros smothered in chile, or my favorite: [Roper's](http://www.vicksvittles.com/) green chile
relleno omelette [^1]. That's a whole green chile, stuffed with cheese and deep fried, which is then stuffed into an omelette
with more cheese, which is then smothered with green chile and (wait for it...) more cheese. It's the turducken of
breakfasts. Hmm, there's an idea...

{{<figure src="/images/green-chile-2017/chile-relleno-omelette.jpg" title="Roper's green chile relleno omelette. I would sacrifice any two of my family members for one of these right now.">}}

If you're not still full from breakfast, lunch presents a multitude of options. You'll find green chile as a topping on
your pizza at any of the pizza places, or as an option for your hamburger at any of the burger places [^2]. Even the chain
restaurants carry it... and if the McD's dollar menu doesn't sound appealing to you, believe me that a quarter-pounder
with cheese and green chile on the way home from the brewpub at 2AM is a damn delicious way to end an evening. Speaking
of, chiles even find their way into the beer; red is fairly common in stouts and porters, while green occasionally makes an
appearance in a few local lagers.

But to see chile in its natural habitat, you have to visit a New Mexican restaurant. Chile is one of the cornerstones of
[New Mexican cuisine](https://en.wikipedia.org/wiki/New_Mexican_cuisine), simultaneously defining it and (unfortunately)
limiting its geographic distribution. It's there that you'll find the distinctly New Mexican versions of the dishes
available at Mexican restaurants everywhere else in the US: green chile stew, carne adovada and other red chile braised
meats, chiles rellenos, Frito pie, red chile chorizo, and chile-filled and -smothered burritos. The showcase, however,
is the red or green chile sauce, so closely tied to chiles and distinct from other salsas that it's simply called
"chile". Chile (the sauce) can be, and is, put on or in anything, but one of the most prominent applications is
enchiladas. An enchilada dinner at a New Mexico restaurant is capable of illustrating many of the differences between NM
and other Mexican regional cuisines: flat enchiladas, often made with blue corn tortillas, stacked with cheese and other
ingredients, covered in chile sauce and cheese and topped with an over-easy egg. You'll be asked whether you want green
or red chile (this is the official state question of New Mexico: "Green or red?"); feel free to order both
("Christmas"). No sour cream (you're in the desert). No guacamole (still in the desert). Obviously no jalepeños. [^3]

So, what makes a New Mexico chile? The term refers to one of a number of cultivars of chiles, grown in the state of
New Mexico. Like grapes, the flavor of the chiles varies according to the soil and the climate. As a result, the chiles
vary from year to year, in the flavor as well as the spiciness. In our relatively short four years (five harvests) in
New Mexico, we experienced this shift from season to season. As the new harvest comes to market in late summer,
restaurants will take special advantage of the fresh chiles. The frozen chiles from the previous season make way, at least
temporarily, and we get a chance to taste the chile harvest that will carry us through to the next year. I remember one
year, I want to think it was 2008, where the increase in spiceness from the previous year was such that the staff at one
of my regular restaurants eventually caved to customer requests and started serving sweet tea. I know, it baffles the
mind of any southerner that a restaurant wouldn't have sweet tea, but that's just how they roll in the 505.

And harvest time is a special time of the year. ABQ is not a farming town; there's no quaint parade of horse-drawn carts
bringing the green chile harvest in. Let me assure you, that would not go over well on the freeway. But there's no
mistaking when it's harvest time. At harvest time, that magical time of the year, the groceries stores start selling
fresh green chile in the produce section. And you don't even have to go into the store to tell that they're for sale.
Because outside of every store that sells green chile---be it a supermarket, a roadside stand, Walmart---there is a
roaster. You buy your chiles fresh, probably a very large sack of them, then you take them outside where the person
operating the roaster throws them in and roasts them until the meat softens and the skin blisters. Then you chuck them
into a trash bag so they can steam while you drive home, at which point you'll peel the chiles.

That time of the year was one of my favorite times of the year in Albuquerque. The heat of the summer is starting to
break, the overnight temperature is starting to flirt with the 50s, and everywhere you drive you see people standing in
line at the roaster, the smell of roasting chiles in the dry air. So, imagine my delight when, a year after moving to Knoxville,
TN, I discovered that the Fresh Market chain of grocery stores had fresh chiles at the Knoxville stores during the
harvest. Every year, they ship the chiles to their stores, each store equipped with a chile roaster. There were no New
Mexican restaurants in Knoxville, but if I went to the right grocer at the right time of year and bought, peeled and
froze a sufficient amount of chile, I would be set for the rest of the year. This was a tradition that continued after
we moved to Tallahassee. Every year, starting in August, I start calling the produce manager at the Fresh Market and
harassing them about the expected date for the delivery of chiles. But not this year. [^4]

{{<figure src="/images/green-chile-2017/chiles-2015.jpg" title="Chiles harvest from 2015, ready for peeling.">}}

This year, when I called the store, the manager informed me of the delivery date for the chiles, but told me that their
roaster, set aside last year to be cleaned, might have "accidentally" been thrown away.  Okay... yellow alert. Don't
panic. It's probably there. I mean, how could you accidentally throw away a three-hundred pound propane roaster? I
called back a week later. Chile delivery is delayed, still haven't found the roaster. I called back the following week,
they hung up on me. I'm just kidding... they're very polite, though I was starting to feel like a stalker. I finally
dropped into the store and was met with the glorious sight of two baskets full of chiles, sorted by spiciness. But they
were fresh, not roasted, and the produce manager that I spoke to confirmed my fear: no roasted chiles this year.

{{<figure src="/images/green-chile-2017/1-basket-of-chiles.jpg" title="Jackpot.">}}

Obviously there's two choices here... roast my own chiles or have no chiles at all. So I filled up a couple of produce
bags with all of the chiles from the hot barrel and went home. I've never roasted chiles before in bulk, only a few at a
time over a gas burner, so I read [this article](https://www.nutmegnanny.com/how-to-roast-hatch-green-chiles/) from
Nutmeg Nanny. The author recommends roasting the chiles on baking sheets in the oven, so that's what I tried to do.

{{<figure src="/images/green-chile-2017/2-lotta-chiles.jpg" title="Attempt #1: Roast them in the oven.">}}

Preheat oven? Check. Crack a window and turn on the fan? Check. Put chiles under oven broiler? Check. And it works! The
skin starts darkening and the house fills with the aroma of roasted chile. And then the kids complain about the smell.
And then the kids start coughing and rubbing their eyes. And then your spouse starts complaining that you're macing the
children. And then you find yourself kicked out of the house.

{{<figure src="/images/green-chile-2017/4-chiles-on-the-grill-2.jpg" title="Attempt #2: Roast them on the grill.">}}

Footnote from Nanny's article: you can also roast them on the grill. Well, duh, Chris... if propane is good enough for
the big roaster in New Mexico, it's good enough for here. So I fire up the propane grill, throw a mess of chiles on,
and wait for them to blister. And here's the real benefit of doing this on the grill (aside from, you know, not sending
your family and pets to the hospital.) On the grill, you have the ability to watch each chile individually, flipping
them as necessary, and moving them off the fire into a bag to steam when appropriate. This is much easier than when
they're in the oven. Also, my grill is bigger than my oven, so I can roast more chiles at once.

After they're all blistered and bagged, and they've had about 30 minutes to steam, it's time for the hard work. First,
you've got to peel them. Then you've got to seed them. Do you have a pair of rubber gloves? Get some rubber gloves.
First, it looks awesome... this is some Dr. Frankenstein, mad-scientist-in-the-kitchen shit right here. Because you're
going to be handling a bunch of chiles, and if you don't wear gloves, two things are going to happen. The first is that
your hands are going to burn; in 2014, I peeled about 20 pounds of chile without gloves and my hands burned for three
days. The second thing is that you're eventually going to rub your eyes. It might be when you take your contacts out, it
might be when you get a little itch. But when you do, some of that capsaicin that's been hanging around is going to make
you regret your life choices.

{{<figure src="/images/green-chile-2017/7-peeling.jpg" title="Peeling chiles (2015). Note the gloves: they're not just sexy, they'll also save your mucus membranes. You care about your mucus membranes, right?">}}

Roll each chile around between your (gloved) fingers. The skin will start to separate, and if you're lucky, the whole
thing will slide right off. If not, you'll have to work at it a bit. Don't worry... you've got a bunch of chile to get
through. You'll get the hang of it. After all the chiles are peeled, cut off the stem, slit each one open and use a
spoon to gently scrape out all the seeds. Don't worry if a few seeds make it into the final product; they'll add a
little extra spice. You just don't want a ton of them, because we went through a lot of trouble to get yummy roasted
chile flesh, not bitter seeds. At this point, you can preserve them however you like. I don't have any use for whole
chiles, so I chop them and vacuum seal them in 1 cup portions.

{{<figure src="/images/green-chile-2017/8-bagged.jpg" title="Peel, chopped, bagged and ready for the deep freeze.">}}

So, now you've got a mess of chile, what do you do with them? I have a finite amount, so I tend to save them for special
occasions. I'll thaw a bag a few times a year and make green chile cheeseburgers for friends. Any chile leftover from
that ends up in huevos rancheros for breakfast (chile and runny egg yolk are best friends) or as a pizza topping. And my
new favorite dish combines one of my favorite foods from the southwest with one of my favorite foods from the south:
green chile, chive and bacon deviled eggs [^5]. I take a dozen or four of those to a family reunion, sidle up to folks and say,
"Let me tell you about the miracle of New Mexico chiles."

{{<figure src="/images/green-chile-2017/green-eggs-and-bacon.jpg" title="South-by-Southwest: green chile, onion and bacon deviled eggs.">}}

[^1]: Roper's is now called Vick's Vittles, but I'm told that the menu is the same. The chile relleno omelette is a rotating special, but they may let you order it off menu. It can't hurt to ask.
[^2]: [Owl Cafe](http://www.salon.com/2011/10/12/atom_bomb_cheeseburger/), FTW.
[^3]: Seriously? Jalepeños? GTFO.
[^4]: 2017 is just a never-ending shit-storm, right?
[^5]: Recipe: Egg yolks, mayo, finely chopped green chile, finely chopped onion, bacon grease, garlic salt (the salt is **necessary** to enhance the flavor of the bacon grease and the green chile). Garnish with bacon and green onion.
