---
title: "Introduction"
date: "2017-01-02T13:25:43-05:00"
categories: 
- news
tags: 
- hugo
- aws
- gitlab
- personal
weight: 1
comments: false
---

Welcome to cgbaker.net, the personal webpage for Chris(topher Grover) Baker. It's part blog and part project warehouse, my own little non-peer-reviewed piece of the internet.

This replaces my previous academic webpage. This is partially out of necessity, as the former site is no longer accessible to me because it was hosted at the [Computer Science and Mathematics Division](http://www.csm.ornl.gov) at [Oak Ridge National Laboraties](http://www.ornl.gov), where I previously worked.
But it also serves to curate a broader set of interests than did the former. In addition to my (very gradual) ongoing research in linear algebra and Riemannian optimization, my intention is to write about cloud computing, dev-ops, programming, homebrewing, and whatever DIY I get around to writing up. 
So feel free to check out the [projects](/project) link in the menu, the blog post [categories](/categories) listing, or my [bio](/bio).<!--more-->

The previous page was written in a horrible PHP "framework" that I wrote as a grad student; I've decided to take a different approach this time around. This one will be all static,
using the Hugo website engine for content generation, AWS S3 and CloudFront for hosting the generated pages, Gitlab for hosting the source and performing continuous delivery. I
wrote a [post](/site-architecture) detailing my approach to these.
