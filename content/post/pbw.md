---
title: "PBW is my best friend"
date: "2017-07-29T09:51:05-05:00"
categories: 
- kitchen
thumbnailImagePosition: left
thumbnailImage: /images/pbw/thumb.jpg
keywords: 
- homebrewing
- sous-vide
- coffee
- tips
tags: 
- homebrew
- sous-vide
- coffee
- tips
---

One of the benefits of engaging in such a broad subject as home brewing is the secondary use for all that equipment and random knowledge. Friend needs to borrow a 7 gallon glass
jug? I have one of those. Brother-in-law needs to rapidly cool down 6 gallons of boiling liquid? I can help with that. Neighborhood lemonade stand wants to know the specific
gravity of their signature beverage? No problem.

To be honest, none of those scenarios have ever happened to me. Maybe I'm not that approachable?

However, I personally have found plenty of use for some of that homebrew accoutrement laying around. And the best example of this has resulted in my love for PBW. PBW, a.k.a., Five
Star's Powdered Brewery Wash, is an "alkaline non-caustic, environmentally- and user-friendly CIP cleaner". Meaning, it's not so professional (read, "dangerous") that you're going
to kill yourself and your family, but it's also strong enough to get the job done. In the brewery (read, "garage"), I use it to clean carboys, kegs, kettles and mash tuns. So, everything. And in the
kitchen, I use it to clean everything else. 

PBW works great to clean the stainless steel equipment in your brewery, so you shouldn't be surprised that it cleans the stainless steel equipment in your kitchen. The
stainless steel carafe from your [Technivorm Moccamaster](https://technivorm.com/products/thermal/kbgt-polished-silver/)? Mix up a batch of PBW, soak it in
there for an hour or so. It's as clean and shiny as the day that you brought it home (which, based on the popularity and price increases over the past five years, was hopefully a long
time ago.) Before you do that, run it through the coffee machine to descale it.

{{<figure src="/images/pbw/carafe.jpg" title="As shiny as the day it was made.">}}

What about the heating coils and shield on your lovely [sous-vide](/tags/#sous-vide-list) circulator? Covered in scale from north Florida aquifer water? Run some PBW through that
old thing at 160°F for about thirty minutes.

{{<figure src="/images/pbw/pbw.jpg" title="Work smart, not hard.">}}

Got a little build-up in the dishwasher? Pour some PBW in there, run it on the hot cycle. It'll be like brand-new. 

Contact lenses getting a little cloudy? Dunk them in some PBW overnight. Just kidding. Don't do that. Maybe that's why nobody asks for my help?
