---
title: "Anova sous-vide circulator, and sous-vide chicken wings"
date: "2017-01-05T14:31:52-05:00"
categories: 
- kitchen
thumbnailImagePosition: left
thumbnailImage: /images/sous-vide-wings/done.tn.jpg 
keywords: 
- sous-vide
- chicken wings
- anova
tags: 
- sous-vide
- cooking
- recipes
imageGallery: true
gallery:
    - /images/sous-vide-wings/b-and-b.jpg /images/sous-vide-wings/b-and-b.tn.jpg "Vacuum seal bag ready for a little something special"
    - /images/sous-vide-wings/circulator.jpg /images/sous-vide-wings/circulator.jpg "Circulator almost ready at 160°F"
    - /images/sous-vide-wings/tray.jpg /images/sous-vide-wings/tray.jpg "Wings out of the water bath, ready to dry in the refrigerator"
    - /images/sous-vide-wings/done.jpg /images/sous-vide-wings/done.tn.jpg "Out from the broiler; the skin could be a little more crispy, but it was time for dinner"
---

I'm a bit of a gadget freak, and this tendancy applies in the kitchen as much as anywhere. I'd been meaning for some time to get a sous-vide circulator, and last May (2016) I finally
broke down and purchased the Anova Precision Cooker.<!--more-->

I bought an 800W model, which has been discontinued in favor of a new [900W model](https://www.amazon.com/Anova-Precision-Cooker-WIFI-Watts/dp/B01HHWSV1S). I work at home, so I
didn't need any of the cloud support of the Wi-Fi model (I rarely activate my sous-vide circulator while driving around town, at Starbucks, etc.). However, I was concerned about
the Bluetooth range not reaching my office upstairs from the kitchen, so I went with the Wi-Fi model. More gadget == more better, right?

Since we bought this machine, I would wager that 95% of the meat cooked in our house has been cooked in this manner. Most of it has been as simple as chicken thighs or beef steaks,
mainly for the convenience and consitency of sous-vide cooking: I can put the meat in mid-afternoon and then walk downstairs at 5ish and finish the product on the grill or the pan 
in a matter of minutes. Coupled with my habit of bulk-buying meat at Costco, vacuum-sealing into meal-sized portions and throwing them in the chest freezer, this makes for some
low-maintenance, morning-of easy meal planning.

I do have a number of favorite recipes (red-chili-rubbed bbq flank steak, bbq chicken thigs) which I'll write up the next time that I cook them. Today, I decided to take first pass
at chicken wings. My initial research for any sous-vide preparation is to Google and see what [Kenji](http://www.seriouseats.com/editors/j-kenji-lopez-alt), patron saint of sous-vide, has to say. 
Chicken wings was one of the rare instances where I was let down; the closest match was an 
[treatise concerning twice-fried chicken wings](http://www.seriouseats.com/2012/01/the-food-lab-how-to-make-best-buffalo-wings-fry-again-ultimate-crispy-deep-fried-buffalo-wings.html), which
contained an aside that the first fry could (should?) be replaced by a sous-vide preparation.

The next compelling recipe I found was Alton Brown's two-punch combo, [par-broiled-then-baked wings](http://www.foodnetwork.com/recipes/alton-brown/buffalo-wings-recipe.html).
The par-broiling, like with Kenji's first fry, serves to cook the meat and gelatinize the collagen; an intermediate period in the fridge allows the skin to dry; and the final step aims to
crisp up the skin in the oven. I'm not opposed to deep frying the wings. I worked as a fry-cook for years at my uncle's restaurant; I have deep-fried many thousands of chicken wings. I have a
wok and a side burner on my grill outside. But I don't fry often enough to justify using that much oil, so I found Alton's oven-finish appealing.

So, my approach is a combination of the two: Kenji's suggestion to ready the wings sous-vide, followed by Alton's crisp-up in the oven. Kenji suggested adding some duck fat to the first
phase. I don't keep duck fat on hand (heaven knows what kind of trouble I would get into there), but I do keep bacon grease. Close enough, right? So, bacon grease and wings in the
sous-vide, at 160°F for two hours, the goal being that (as with confit), the well-done result of the meat is offset by all of the rendered collagen. A few hours in the fridge to
dry the skin, then into the oven at 425°F (per Alton) for 20 minutes on each side to crisp up the skin.

Unfortunately, I forgot to set a timer and got carried away with work and didn't have time for that. So I did about 10 minutes on each side, plus a few minutes with the broiler on
high. They crisped up a bit (see gallery below) and then I tossed them in a nice Pete-butter mixture. 

The result? Pretty good. The skin could have been more crispy. Next time, I'll make sure to leave myself time to give them a proper go in the oven. Or maybe I'll break down and
finish them in the grease. The meat was well-done, as wings can sometimes be, but not dry. Still, I think I might try lowering the temperature a little bit next time, maybe to
155°F. I do have the rest of a bag of frozen chicken wings in the deep freeze.

