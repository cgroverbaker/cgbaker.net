+++
date = "2017-01-03T15:57:32-05:00"
title = "New site architectural overview"
draft = false
aliases = ["/site-architecture"]
categories = ["cloud"]
tags = ["continuous-delivery", "gitlab", "aws", "hugo", "devops", "docker"]
+++

As I promised in the [intro]({{< relref "/post/intro.md">}}), here's a breakdown of the software and processes used to construct and delivery the site:

- [Hugo](https://gohugo.io): flexible static site generation from markdown
- [Gitlab](https://about.gitlab.com): version control for site source, continuous delivery system
- [Amazon Web Services (AWS)](https://aws.amazon.com): static content hosting, load balancing, content distribution, certificate management

<!--more-->

*Update: I've [updated the site architecture]({{< ref "site-architecture-update" >}}) to host on Cloudflare Pages instead of AWS.*

In a nutshell: 

1. The source repo [lives in Gitlab](https://gitlab.com/cgroverbaker/cgbaker.net/tree/master).
2. I update the site by updating/creating new markdown files for posts and projects.
3. The changes are pushed to Gitlab, which kicks off the [Gitlab CI runner](https://docs.gitlab.com/ee/ci/README.html). 
  * In most of my other projects, this would run tests. But my website doesn't have any tests (*yet?*), so its just a matter of building and publishing.
  * The gitlab CI runner launches a [Docker](https://www.docker.com/) container which builds the static content from the markdown.
  * It pushes the static content up to an [Amazon S3](https://aws.amazon.com/s3/) bucket which hosts the site.
4. [Amazon Route 53](https://aws.amazon.com/route53/) DNS sends *cgbaker.net* traffic to [Amazon CloudFront](https://aws.amazon.com/cloudfront) 
   instances, which terminates SSL and caches the content stored in the S3 bucket.
5. Magic happens in your browser.

When I set out last week (on vacation) to design the site, I knew I wanted a site with static content hosted in S3, using Gitlab CI (:heart::heart::heart:) 
for deployment. When I started Googling around for answers, I found the following post: http://www.rjocoleman.io/post/gitlab-s3-deployment/. I looked into Hugo, 
decided it would do what I wanted, and then ripped off the instructions at that site.

So, what's different from [Robert Coleman](http://www.rjocoleman.io)'s excellent :thumbsup: setup? Not a lot. I have http://cgbaker.net 301 redirecting (per [this post](https://aws.amazon.com/blogs/aws/root-domain-website-hosting-for-amazon-s3/)) to
http://www.cgbaker.net. I have CloudFront configured to redirect HTTP to HTTPS. I've started tweaking the [Tranquilpeak](http://themes.gohugo.io/hugo-tranquilpeak-theme/) theme to 
my liking (initially, adding support for archetype **project**, in addition to the native **post** archetype). 

I also use a [custom docker image](https://hub.docker.com/r/cgbaker/alpine-hugo-aws/) based on [Alpine Linux](https://alpinelinux.org/) with git, hugo and aws-cli built-in. The
image is small (fast to download at build time) and has all the necessary tools baked in. The source for the image is available
[here](https://github.com/cgbaker/docker-alpine-hugo-aws). It is configured at Docker Hub as an [automated build](https://docs.docker.com/docker-hub/builds/), so that whenever I update the repo in Github (they don't support
Gitlab yet), the image is automatically built and made available in the Docker Hub.

The Gitlab CI [pipeline](https://gitlab.com/cgroverbaker/cgbaker.net/blob/master/.gitlab-ci.yml) is super small, check it out. Later on I may add
support in the pipeline for [Gitlab Review Apps](https://about.gitlab.com/2016/11/22/introducing-review-apps/), just for :poop:&nbsp;&nbsp;and&nbsp;:laughing:&nbsp;.

Feel free to post questions in the comments below.

_Update:_
I updated the CI script to explicitly perform an invalidation of the CloudFront origins on each site update, so that I can keep the TTL at a more reasonable length. This was
inspired by [a post](https://stormpath.com/blog/ultimate-guide-deploying-static-site-aws) from the StormPath team.
