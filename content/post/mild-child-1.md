---
title: "Return to brewing: English Dark Mild"
date: "2017-02-04T18:48:52-05:00"
categories: 
- homebrew
thumbnailImagePosition: left
thumbnailImage: /images/mild-child-1/specialty.tn.jpg
keywords: 
- homebrew
- dark mild
- craft beer
- all-grain
- kegging
- session beers
tags: 
- homebrew
- beer
- session beers
imageGallery: true
gallery:
 - /images/mild-child-1/starter-boil.jpg /images/mild-child-1/starter-boil.jpg "Boiling the starter"
 - /images/mild-child-1/starter-post-boil.jpg /images/mild-child-1/starter-post-boil.jpg "I may have boiled that starter a little hard..."
 - /images/mild-child-1/specialty.jpg /images/mild-child-1/specialty.tn.jpg "Specialty grains, all measured out"
 - /images/mild-child-1/milling.jpg /images/mild-child-1/milling.jpg "The manual mill with a small hopper may have been a mistake"
 - /images/mild-child-1/blow-siphon.jpg /images/mild-child-1/blow-siphon.jpg "How hard can you blow?"
 - /images/mild-child-1/siphoning.jpg /images/mild-child-1/siphoning.jpg "Siphoning into the keg."
 - /images/mild-child-1/too-much-beer.jpg /images/mild-child-1/too-much-beer.jpg "There is such a thing as too much beer."
 - /images/mild-child-1/pour.jpg /images/mild-child-1/pour.jpg "The first pour. Or was it the fourth?"
---

We moved to Tallahassee in 2013. Between a new baby and a new startup, I ended up taking a break from homebrewing. For over three years, as it happened. Last year, I started again
as assistant brewer for a friend, [@ChrisAsa1](https://twitter.com/ChrisAsa1), who is much more dedicated, to the extent that it eventually shamed/inspired me into cleaning up my
equipment. Three weeks ago, I decided it was time to end my dry spell, as it were.<!--more-->

Among other motivations, homebrewers were previously compelled to make the beers that they didn't have retail access to. But for the most part, the dark days of craft beer are
behind us. Even in Tallahassee, there are nearly half a dozen local breweries producing excellent beers across a wide variety of styles, and America's love for craft beer means
that almost any style is available within a short drive to the package store. While that might remove some of the urgency in homebrewing, it also removes some of the constraints. For example, [Proof
Brewing](http://www.proofbrewingco.com/) produces their excellent La-La Land just down the road, so that I don't feel a particularly strong compulsion to make a West Coast IPA. Which isn't to say
that I *won't* make one; imitation is as strong a motivator in brewing as any other artistic endeavor, both for the purpose of testing technique as well as towards flattery. In fact, it's
possible that I'll never make as many batches of any recipe as a friend and I did in Albuquerque with our Sierra Nevada Pale Ale clone.

Having said all that, there are some beers that are still difficult to find. Partially a result of economics, partially a consequence of the realities of long-term storage, there are
a dearth of commercially distributed examples of proper [session beers](https://www.beeradvocate.com/articles/653/). And when I say session beer, I don't mean a 6% IPA. I mean a proper low-gravity, low-ABV,
grab-a-few-on-the-way-home-from-work-and-not-get-wasted beers. If it happens to come out low-calorie at the same time---very likely, since I'm not in the practice of making cola---all the
better. The reality is, if you put a kegerator full of beer a dozen footsteps from where I watch TV or finish my long-distance runs, there's a good chance I'm going to have a few.
As such, I tend to lean towards low(ish)-calorie, low-alcohol beers.

When I first started brewing on my own after leaving Albuquerque for Knoxville, I made a few decisions. The first was that I would do all-grain, full-boil batches. This
wasn't to save money (though bulk grain buying does permit reduction in batch costs), but because it seemed like more fun. I'm doing this largely for fun, right? The next decision
was single-vessel fermentation and kegging. St. Jamil says there's generally no good reason to move from a "primary" (typically, a bucket) to a "secondary" in a carboy. Best case
scenario, you *don't* violate sanitization. So, straight into the carboy, under temperature control, and from there into the keg. This eliminates racking day from my brew calendar,
and it means that "bottling" day involves cleaning a keg and racking into it. The latter of these was actually a hard requirement imposed by my spouse; she hates bottling day more
than anything, most likely a result of it being the last of three consecutive weekends of brewing activites. 

Having laid down the requirements, I had to decide what I wanted to brew. On a diet at the time, and a born contrarian recovering from (and still somewhat offended by) the American
fascination with 15% ABV, 120 IBU IPAs, I decided to go the opposite direction and make low-gravity beers. Because, let's be honest, any asshole can hide their mistakes behind a
1.120 stout or an imperial Saison containing half the spice rack. 

After taking a year building my brewery from the daily sales at [Homebrew Finds](http://www.homebrewfinds.com/), I ended up only brewing three batches before we left Knoxville: 
a low-gravity pumpkin beer for Thanksgiving, based on a English brown ale (with roasted pumpkin, yum!); and a two batches of a low-gravity
[S.M.A.S.H.](http://beersmith.com/blog/2012/10/24/smash-brewing-single-malt-and-single-hop-beers/) [saison](http://www.bjcp.org/2008styles/style16.php#1c) (the recipe had about
8oz of Caramunich on a base of Pilsner malt, so it wasn't technically a single malt). The pumpkin beer was pretty good, but I liked the saison a lot more.

So, starting up again here in Tallahassee, I decided I'd stick with low-gravity brewing. More recently a fan of English styles, I decided to go for an [English dark
mild](http://www.bjcp.org/2008styles/style11.php#1a). Like the saison and the brown/pumpkin beer before, this has the benefit of being a style that originates according to the
session parameters. A little Googling found Jamil's [style write-up at BYO](http://byo.com/body/item/1893-mild-ale-style-profile). I dialed the OG down just a hair, slightly
increasing the proportion of specialty grains, consistent with JZ's advice on low-gravity beers (recipe [here](/recipes/mild-child-1.txt)). I made a 500ml starter with [WLP002](http://www.whitelabs.com/yeast-bank/wlp002-english-ale-yeast), about 24 hours on a stirplate in my fermentor
at 68°F. I mashed high at 156°F (relative to JZ's 154°F), batch sparged (manually vorlaufing each batch), and boiled for 60 minutes. The OG post-boil ended up at 1.037, a few points higher than the 1.035 target. In
hindsight (see below), I should have boiled off a little more, because I ended up with more finished beer than would fit in a corny keg, by about half a gallon. Those extra
points and melanoidins certainly would not have hurt the beer I ended up with. 

<iframe width=100% height=350 src="/recipes/mild-child-1.txt"></iframe>

It went into the fermentor on Sunday around 5:00 pm at 67°F, just in time to finish off the ribs for my assistant brewer and families. Tuesday afternoon (~42 hours), my
refractometer reading indicated I was at 1.015 and 2.82% ABV. I turned the temp up to 69°F to encourage some ester formation from the WLP002. On Thursday afternoon (~60 hours), the
refractometer indicated 1.012 and 3.22% ABV. I got busy and "forgot" about the beer for a week and, almost two weeks after brewing, "crashed" it to 55°F (the desired storage
temperature). On Feb 11, a day short of three weeks, I cleaned out my kegerator lines (nasty, after three years) and a keg (using the wonderful [Mark II Keg/Carboy
washer](https://www.morebeer.com/products/mark-ii-keg-carboy-washer.html)) and racked the beer. I set the temp on the kegerator to 55°F and the keg to 10psi. Final gravity, via
hydrometer, was 1.012, same as two weeks prior (the sample was noticibly cleaner tasting than the sample from four days in).

{{<figure src="/images/mild-child-1/too-much-beer.jpg" title="There is a such thing as too much beer.">}}

I ended up with more beer than would fit into my corny keg; I kept about 32 oz, which I threw into the fridge. The beer was good; uncarbonated and at 60°F, I felt
there might have been a little too much black patent malt. Today, one day in the keg and with some carbonation, it's smoother. I dropped the temp on the kegerator to 53°F
(overshooting JZ's recommenation of 1.5 vols CO2 to about 1.8). As of Sunday, I'm about three pints in (session beer, ftw!) and I am
pretty satisfied.

Some of this equipment and procedure were new for this batch, some are repeats. What are the lessons learned? 

* This was my first batch of beer with a pump, but some of the plumbing
  and electrical I needed showed up on my doorstep literally during the boil, so I wasn't able to fully utilize it. I'm looking forward to giving fly sparging a try, only for the 
  reason that I can use the pump to vorlauf my mash once, instead of having to do it after every sparging batch. I will say that the time to cool the wort using my immersion chiller
  definitely seemed less using the pump to recirculate/whirlpool; however, I'm unlikely to try without it for science's sake, so I may never know how much it helped.
* I am definitely considering getting a nicer grain mill. Trying to save as much money as possible, I initially purchased [this manual
  mill](https://www.morebeer.com/products/victoria-grain-mill.html). While it gets the job done, it is finicky to set up and adjust, and it takes forever to mill even the small
  amounts of grain in my low-gravity batches.
* This was my first yeast starter over fire, using the side-burner on my grill. Previously I had used a double-boiler technique on my (electric) kitchen range. I didn't care for
  the double boiler; the boil wasn't very vigorous and the flask does a disturbing amount of rattling. The grill-side experiment, however, 
  was a gigantic mess; the side burner doesn't go low enough to prevent significant boil-over. I lost over 20% of my starter volume from the boil-over, and I ended up cutting the 
  boil off before the traditional 10 minutes. I bought a vial of [Fermcap](https://www.amazon.com/Fermcap-S-Foam-Inhibitor-2-oz/dp/B0064OMA54), which I'll use next time with the
  starter.

**Update #1**: On Monday after 24 hours at 53°F, it was even better; a little less harshess from the specialty grains, a appearance of head and a hint of carbonic bite. I dropped the
temp by another 1°C to ~51°F, leaving the CO2 at 10 psi. This corresponds to about 1.87 volumes CO2, still high in the range of JZ's 1.5 - 2.0 recommendation.

**Update #2**: I think that's the sweet spot. 51°F and 10psi. A brief appearance of foam, a little bite, pretty easy drinking. Next time, I think I will reduce the amount
of black/patent malt (I should note, Jamil called for 200° pale chocolate malt and I used a roastier 350° chocolate malt, which is what I had on hand).

