---
title: "Marianna Green Chile Harvest 2019"
date: "2019-10-05T17:43:00-05:00"
categories: 
- kitchen
thumbnailImagePosition: left
thumbnailImage: /images/green-chile-2019/9_sauce.tn.jpg
keywords: 
- green chile
- hot sauce
- fermentation
tags: 
- green chile
- cooking
- recipes
- fermentation
- hot sauce
---

I missed out on the green chile harvest in 2018, so you can imagine that the small amount of chile I
[put away in 2017]({{< ref "/post/green-chile-2017" >}}) was gone. Someone in the #talk-food channel
in the work Slack mentioned that the chiles had come in, so I struck hard and fast this
year.<!--more-->

A few things conspired to complicate the roasting effort this year. The first is that we moved from
Tallahassee to our home town of Marianna, about an hour away. As such, procuring green chiles
requires driving to Tallahassee. Fortunately, between the myriad appointments that we still keep in
TLH, it was just a matter of swinging by the Fresh Market while in town. The season nearing its end,
I asked Kelly to grab some on her next trip; she asked how much, and I said "a ton". She came back
with a heaping bag of green chile, weighing in at four pounds. I'm thinking, this is good. This
seems like a lot.

{{<figure src="/images/green-chile-2019/0_fresh.jpg" title="This is 4 pounds of green chile.">}}

The second complication was that the propane grill I previously used for roasting had rusted out and
was abandoned in the move. However, I have since aquired a Traeger pellet grill. So, this year, the
chiles will be wood-fire roasted instead of gas roasted. I was excited to see what the difference
would be, in terms of flavor and the quality of the roast.

{{<figure src="/images/green-chile-2019/1_roast0.jpg" title="Fresh chiles on the grill.">}}

I set the grill to 250°F, threw on the chiles, and set a timer to check on them every five minutes.
As they finished, I would move them into a sealed ziplock bag to steam and add more chiles to the
grill. They certainly smelled good, but the skin didn't have the papery, blistery quality I
expected, and I was a little concerned about the difficulty in peeling them.

{{<figure src="/images/green-chile-2019/2_roast1.jpg" title="Roasting in process.">}}

Fortunately, they peeled up just fine. I seeded and chopped them, then measured them out into vacuum
seal bags, where I learned that 4 pounds of fresh green chiles and three hours of work yields a
little over 3 cups of roasted and chopped chiles. Suffice it to say, I was a little disappointed. 

{{<figure src="/images/green-chile-2019/4_pack.jpg" title="This is also 4 pounds of green chile.">}}

So, on my next trip to Tallahassee, about a week later, I hopped into the market and was pleased to
discover that there were still fresh chiles available. At which point, I bought about ten pounds. I
didn't have time to roast them right away (I was leaving the next day for HashiConf.) I had read
about [fermented pepper
sauce](https://nourishedkitchen.com/fermented-hot-chili-sauce-recipe/) and wanted to give it a try,
so I grabbed one of my larger glass jars, mixed up a brine, and added carrots, onions, garlic, and
chiles. I don't have any pickling weights (yet), so I used a quart size ziplock full of water. I 
used saran wrap secured with a rubber band for an airlock, just like my mom and granddad taught me
for homemmade wine. 

{{<figure src="/images/green-chile-2019/6_ferment1.jpg" title="Chiles and veggies in the brine.">}}

I set the jar inside a baking dish in the utility room in case of overflow. [_Narrator: there was
overflow._] Then I hopped on a plane to Seattle for a week. When I came back, the side of the house
containing the utility room was redolant of garlic and general funkiness, to the extent that my
spouse had deployed the air purifiers. I took a peek at the progress and was upset to see what
looked like blue mold on the garlic. I didn't feel like dealing with it, so I ignored it for a few
more weeks. 

{{<figure src="/images/green-chile-2019/7_strain.jpg" title="Strain the pickles to reserve some brine.">}}

After about three weeks, tired of the lingering garlic smell, I carried the jar out to
the garage and began straining off the brine. Fortunately, what I thought was blue mold was just
distortion through the glass jar and [a
reaction](https://www.thespruceeats.com/garlic-turns-blue-when-pickled-1327752) to the acidic
environment. The brine tasted great, with a nice level of tartness and a somewhat deep funkiness.

{{<figure src="/images/green-chile-2019/8_blend.jpg" title="Blend on high. Repeatedly.">}}

I blended the ingredients in batches, adding enough brine to give the sauce a pourable consistency.
I don't mind a chunkier sauce, so I didn't strain out the remaining solids. I pour the sauce into
growlers for temporary storage, until such time as I could find some smaller bottles for
distribution. The sauce is not quite as spicy as I had hope for, but it does have a "warmth".
Furthermore, the sourness has a lot more dimension than your typical hot sauce with added vinegar.
I'm looking forward to seeing how this sauce evolves over the coming months.

{{<figure src="/images/green-chile-2019/9_sauce.jpg" title="Almost one gallon of fermented Hatch chile sauce.">}}

