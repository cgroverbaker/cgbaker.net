---
title: "Site Architecture Update"
date: 2025-02-17T14:06:22-06:00
categories:
- cloud
tags:
- aws
- hugo
- cloudflare
thumbnailImagePosition: left
thumbnailImage: /images/cloudflare-migration.png
---

After experimenting with some other sites, I've shifted the hosting for my website from AWS S3 to Cloudflare Pages.

<!--more-->

As described in my [original architecture post]({{< ref "new-site-architecture"
>}}), this site is generated using the [Hugo](https://gohugo.io) static site
generated, which allows for cheap and efficient hosting. Previously, this
hosting was done on AWS; the site was pushed to S3, and hosting was performed
by [AWS CloudFront](https://aws.amazon.com/cloudfront). I had almost no
problems with this approach; amusingly, there were multiple large-scale events
over the past 8 years where large parts of Amazon and/or AWS pages were down,
but my website would still be up.

However, when I launched the site for [Flame Point Brewing]({{< ref
"flame-point-brewing" >}}) a few weeks ago, I decided to host it on [Cloudflare
Pages](https://pages.cloudflare.com/) instead of AWS. This was initially done
as an experiment to get familiar with Cloudflare's product suite. An additional
benefit is that my hosting needs matched Cloudflare's free tier (although AWS
was only costing me about $18/year). However, the main benefit was a simplified
CI/CD process as a result of moving to an opinionated PaaS.

As noted [before]({{< ref "new-site-architecture" >}}), the build and deploy of
this site is performed using GitLab pipelines. When I push updates to the
GitLab repo, GitLab fires up a CI runner using the Docker image that I specify;
this image uses Hugo to build the static content, pushes the static content to S3, then 
instructs CloudFront to invalidate their caches.

The result is that my build pipeline explicitly specifies the following:
* a docker build environment, including the version of Hugo that I want to use;
* credentials to talk to AWS;
* the pipeline commands to build and publish the website; 
* AWS infrastructure to manage certs and domains.

Aside from updating the docker image, most of these were one-and-done efforts.
I could hypothetically update the docker image to fetch a preconfigured and/or
specified version of Hugo, at which point I wouldn't need to update this image
anymore... because at that point I would have finished building a Hugo PaaS for
CloudFront.

So, I decided to use an existing PaaS. As noted on the [Hugo
site](https://gohugo.io/hosting-and-deployment/), there are a number of
providers which support integration with Hugo. Among these is [AWS
Amplify](https://gohugo.io/hosting-and-deployment/hosting-on-aws-amplify/),
which launched a few years after I built this site. Cloudflare Pages provides
an opinionated PaaS for Hugo, whereas Amplify allows sufficient expression of
the pipeline in order to support Hugo. I don't need the flexibility, so I opted
for Cloudflare pages and moved all of my hosting (registration, DNS, email,
etc) for this domain over to Cloudflare.
