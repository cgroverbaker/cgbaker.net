+++
date = "2017-01-16T18:09:21-05:00"
title = "MathJax support for rendering equations"
name = "mathjax-support"
math = true
categories = ["math"]
tags = ["hugo", "MathJax", "LaTeX", "test-driven development", "singular value decomposition", "block Rayleigh quotient", "Riemannian gradient", "Stiefel manifold"]
keywords = ["hugo", "MathJax", "LaTeX", "singular value decomposition", "Riemannian gradient"]
+++

*Update*: Tranquilpeak has [added built-in support](https://github.com/kakawait/hugo-tranquilpeak-theme/pull/294) for MathJax.

If I'm going to occasionally write posts about math (and that is the plan), I need the ability to render equations. It's not a strict necessity; you can get pretty far by literally
describing what it is that you're trying to say. For example:

> The Riemannian gradient over the compact Stiefel manifold of the Rayleigh quotient with respect to some square matrix and a given conformal subspace can be computed as the
> orthogonal complement with respect to a basis for that subspace of the product of said matrix and said basis.

However, though it may not be worth a thousand words, an equation in this case is worth approximately fifty words: 

`$$ \nabla\ \textrm{RQ}_A(\textrm{colspan}(V)) = (I - VV^T)AV $$`

Or, appropriately rendered:

$$ \nabla\ \textrm{RQ}_A(\textrm{colspan}(V)) = (I - VV^T)AV $$ 

Fortunately, there are more solutions for this than the last time I looked, circa 2007. And if you Google ["math, latex and
hugo"](https://www.google.com/search?q=math+latex+hugo) (as I did), you find the following on the official Hugo website: [MathJax support in
Hugo](https://gohugo.io/tutorials/mathjax/). 

And so, because I'm a fan of test-driven development, I wrote up this post with a little math (i.e., the block math above) and this inline
[SVD](http://mathworld.wolfram.com/SingularValueDecomposition.html) here: $A v_i = u_i \sigma_i$. And because I hadn't gone through the steps in the post above, it looked like shit
(i.e., the test failed): 
{{<figure src="/images/mathjax/before.png" title="This post before MathJax support">}}

But when you add in the steps from the article to add MathJax support to all pages (e.g., in the footer), it's *mostly* good. My text editor (vim) still doesn't understand the LaTeX subscript underscores in the markdown files:

{{<figure src="/images/mathjax/editor.png" title="Vim thinks this is bad markdown">}}

But I can deal with that in a few ways: ignore it, or tell vim not to do syntax highlighting on markdown files (`set filetype`). I'll update this post if I find a better solution.
But in the meantime, I don't have any excuse not to do math posts.

*Update*: MathJax pops up obnoxious loading/processing messages in the browser while it is loading. You can disable this by setting `messageStyle` to `'none'`, like so: 
```
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  messageStyle: 'none',
  showProcessingMessages: false,
  tex2jax: {
    // tex2jax config...
  }
});
</script>
```
