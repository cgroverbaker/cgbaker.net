+++
date = "2017-01-02T15:52:59-05:00"
title = "A short bio"
name = "bio"
comments = false
clearReading = false
aliases = ["/bio"]
actions = false
+++

Chris has been a senior software developer in academia, industry and government for over 20 years. Previously a scientist for the US Department of Energy, he developed software for
the world’s largest supercomputers and published research in leading international journals. At ServiceMesh, and later CSC, Chris worked to streamline development and IT operations
for numerous Fortune 1000 companies. After developing and leading the [Nomad](https://www.nomadproject.io/) ecosystem team at [HashiCorp](https://www.hashicorp.com/), Chris joined Amazon Web Services as a Principal Engineer in the Serverless Containers organization. Chris holds a Ph.D. in Computational Science from Florida State University.

<!--more-->
